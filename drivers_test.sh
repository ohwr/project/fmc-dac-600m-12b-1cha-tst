#!/bin/bash


rmmod fmc_adc_100m14b   2> /dev/null
rmmod zio               2> /dev/null
rmmod rawrabbit         2> /dev/null
rmmod spec              2> /dev/null
rmmod fmc               2> /dev/null

prg=$0
if [ ! -e "$prg" ]; then
  case $prg in
    (*/*) exit 1;;
    (*) prg=$(command -v -- "$prg") || exit;;
  esac
fi
dir=$(
  cd -P -- "$(dirname -- "$prg")" && pwd -P
) || exit
prg=$dir/$(basename -- "$prg") || exit 

top=`dirname -- "$prg"`


insmod "$top/software/fmc-bus/kernel/fmc.ko" 
insmod "$top/software/spec-sw/kernel/spec.ko" "fw_name=../../$top/gateware/spec-init.bin-2012-12-14"
insmod "$top/software/fmc-adc-100m14b4cha-sw/zio/zio.ko"
insmod "$top/software/fmc-adc-100m14b4cha-sw/kernel/fmc-adc-100m14b.ko" "gateware=../../$top/gateware/spec-fmc-adc-v4.0.bin"

adc=`python -c "import sys; sys.path.append('$top/python'); import spec_identification; spec_identification.adc_bus()"`
dut=`python -c "import sys; sys.path.append('$top/python'); import spec_identification; spec_identification.dut_bus()"`
echo "Please verify if both SPEC cards were correctly identified"
echo ""
echo "Files belowe are named with following pattern:"
echo "[arbitrary lenght]-[2 numbers][2 numbers]"
echo "<name>-<bus><slot>"
echo ""

echo "ls /sys/bus/fmc/devices/"
ls  /sys/bus/fmc/devices/
echo ""

if [ "$adc" -ge 0 ]
then
    echo "FmcAdc100m14b was found on bus $adc" 
else
    echo "FmcAdc100m14b was not found"
    echo "Make sure that single FmcAdc100m14b board is mounted in PCIe slot and that it contain valid eeprom content"
fi

if [ "$dut" -ge 0 ]
then
    echo "Board under test was found on bus $dut"
else
    echo "Board under test was not found"
    echo "Make sure that card is mounted in PCIe slot"
fi

