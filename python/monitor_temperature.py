    #!   /usr/bin/env   python

# Copyright Creotech SA, 2015
# Author: Marek Guminski <marek.guminski@creotech.pl>
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org


import sys
import time
import os
import time
import datetime
    

from spec import *
from ad9516 import *
from dac import *


from ptsexcept import *
from utilFunctions import *
from spec_identification import *


FIRMWARE_PATH = '/../gateware/syn/spec_top.bin'


def main (card=None, default_directory='.',suite=None, configure_fpga=True):

    specid = spec_identification()[0];
    if specid < 0 :
        raise PtsCritical("Bus number of FmcDac600m12b1chadds was not found")

    carrier=spec( FIRMWARE_PATH, specid, configure_fpga );
       

    sys_pll = IC9(carrier)
    
    dacinst = dac(carrier)
    dacinst.set_frequency(10e6)

    vcxo_pll = IC26(carrier)
    cwd=os.path.dirname(os.path.realpath(__file__))
    vcxo_pll.write_config(load_config(cwd+'/../doc/new_ad9516_pts_test.stp'))


    vcxo_pll.check_lock()
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d_%H%M%S')
    fh = open("temp_%s.csv"%st,"w")
    fh.write("%f" % carrier.temp.read_temperature())
    while(1):
        fh.write(",%f" % carrier.temp.read_temperature())
    
    return 0
        
if __name__ == '__main__' :
    
    import argparse

    parser = argparse.ArgumentParser(description='Start monitor temperature')
    parser.add_argument('-p', action='store_true')
    args = vars(parser.parse_args())

    if args['p']:
        main(configure_fpga=False)
    else:
        main()

