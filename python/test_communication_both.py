import sys
import time
import os


from spec import *
from ptsexcept import *

from utilFunctions import *

from spec_identification import *


UIDEL = 1;
FIRMWARE_PATH = '/../gateware/syn/spec_top.bin'
INPUT_TIMEOUT  = 30;



def test_sdbid(dut):
    
    print "Checking if SPEC firmware has SDB magic number in register address 0."
    if (dut.sdbid != dut.SDB_SIG ):
        print "ERRRRRORRRRRR!!!!!!!";
        print "ERRRRRORRRRRR!!!!!!!";
        print "SDB signature mismatch!";
        print 'expected SDB: %x' % dut.SDB_SIG;
        print 'found SDB: %x' % dut.sdbid;
        
    else:
        print "SDB magic matched!";
names = [ "FmcDac600m12b1chadds", "FmcAdc100m"]

for i in [0, 1]:
    print "Checking: " + names[i]
    specid = spec_identification()[i];
    print "It was identified on bus:" + str(specid)
    if specid < 0 :
        raise PtsCritical("Bus number of "+ names[i] + " was not found")
    
    carrier=spec( FIRMWARE_PATH, specid, True );
    test_sdbid(carrier)
    print "---------------------------------"
    print "---------------------------------"
    print "---------------------------------"


