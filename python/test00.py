#!   /usr/bin/env   python

# Copyright Creotech SA, 2015
# Author: Marek Guminski <marek.guminski@creotech.pl>
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org


import sys
import time
import os


from spec import *
from ptsexcept import *

from utilFunctions import *

from spec_identification import *



# Quick test designed to ensure that communication with carrier is possible

"""
test00: 
    load firmware
    check communication with SPEC board
    check if correct gateware is programmed
    check FMC presence
"""


UIDEL = 1;
FIRMWARE_PATH = '/../gateware/syn/spec_top.bin'
INPUT_TIMEOUT  = 30;

def test_sdbid(dut):
    
    print "Checking if SPEC firmware has SDB magic number in register address 0."
    if (dut.sdbid != dut.SDB_SIG ):
        print "SDB signature mismatch!";
        print 'expected SDB: %x' % dut.SDB_SIG;
        print 'found SDB: %x' % dut.sdbid;
        raise PtsCritical("SDB signature mismatch! Communication or programming of SPEC might have failed.");
    else:
        print "SDB magic matched!";

    
def test_gwid(dut):        
        
    print "Checking if SPEC firmware has correct GW ID."
    
    if (dut.gwid != dut.GW_ID ):
        print "GW identifier mismatched";
        print 'expected ID: %x' % dut.GW_ID;
        print 'found ID: %x' % dut.gwid;
        raise PtsCritical('GW ID mismatch! Wrong firmware might have been programmed on SPEC.');
    else:
        print "GW identifier matched";

def test_communication(dut ):
        
    print "Checking communication with SPEC by writing register that controls LEDs on SPEC."

    dut.ledr( 0 );
    dut.ledg( 0 );
    
    print >> sys.__stdout__, "Please verify if both SPEC LEDs are off [y/n]"
    tmp = yesnoquestion("", INPUT_TIMEOUT )
    
    
    if( tmp == 0 ):
        raise PtsError('SPEC communication failure. LEDs didn\'t turn off');
    if( tmp == -1 ):
        raise PtsError('User didn\'t verify SPEC LED control test');
    
    
    dut.ledr( 1 );
    dut.ledg( 1 );
    print >> sys.__stdout__, "Please verify if Both SPEC LEDs are on [y/n]"
    tmp = yesnoquestion("", INPUT_TIMEOUT );
    if( tmp == 0 ):
        raise PtsError('SPEC communication failure. LED didn\'t turn on')
    if( tmp == -1 ):
        raise PtsError('User didn\'t verify SPEC LED control test');

    print 'SPEC communication test was successful';

  
def verify_fmc_present(dut):
    print "Checking if a FMC is connected to the SPEC."

    if (not dut.fmc_present):
        raise PtsCritical('FMC card was not detected. Either FmcDac600m12b1chadds is not connected to FMC port or PRSNT_M2C_L line is not connected to GND on FMC board.')
    else:
        print 'FMC card detected'
     

def main (card=None, default_directory='.',suite=None):
  
    specid = spec_identification()[0];
    if specid < 0 :
        raise PtsCritical("Bus number of FmcDac600m12b1chadds was not found")

    carrier=spec( FIRMWARE_PATH, specid, True );
    
#     Verification whether communication with SPEC is possible and if firmware is correct
#     SDB magic number verification
    test_sdbid(carrier)

#     gateware ID verification
    test_gwid(carrier)
    
#     check if writing is possible by LED blinking 
    test_communication(carrier )

#     verify FMC presence (fmc_present pin)
    verify_fmc_present(carrier)
    
       
    return 0
        
if __name__ == '__main__' :
    main()

