

import sys
import random
import time
import math
import os
import signal

from ctypes import *
from ptsexcept import *

from regs_lib import *


#     print massage that have to be logged
def log( msg ):
    print msg

#     print question for user - doesn't need to be logged
def info( msg ):
    print msg
    
 
class dac:
    
    SHAPE_CONST = 0;
    SHAPE_SINE = 2;
    SHAPE_SQUARE = 3;
    
    
    def __init__(self, carrier):
        
        self.carrier = carrier;
        #initial values for fpga dds core
        
        self.sample_rate = 500e6
        
        self.phase_shift_gain = 0;
        self.carrier.writel(DDS_REG_GAIN, self.phase_shift_gain );
        
        self.set_en( 1 );
        self.set_shape( self.SHAPE_SINE );
        self.set_ampl( 1.0 );
        self.set_frequency( 10e6 );
        
        
    
    # enable/disable dds output    
    def set_en(self, val):
        tmp = self.carrier.readl(DDS_REG_DAC );
        tmp = wbsetval(tmp, DDS_DAC_EN(), val);
        self.carrier.writel( DDS_REG_DAC, tmp );
        self.en = val; 
        
    def set_shape(self, val):
        tmp = self.carrier.readl(DDS_REG_DAC );
        
        tmp &= ~DDS_DAC_TYPE_MASK();
        tmp |= DDS_DAC_TYPE_W(val);

        self.carrier.writel( DDS_REG_DAC, tmp );
        
        self.shape = val;
    
    # set output amplitude 
    # val should be float in range [0,1]
    def set_ampl(self, val):
        
        # amplitude value limitation
        if (val>1) | (val<0):
            raise ValueError("set_ampl accepts values in range [0,1]")

        # conversion of voltage to bit value
        val *= float(0x3FFF);
        val = int( val );
        self.set_ampl_bit( val )
    
    # set output amplitude
    # cal should be integer in range [0, (2**14)-1]
    def set_ampl_bit(self, val):
                
        # read current register value
        tmp = self.carrier.readl(DDS_REG_DAC );
        
        # update amplitude value
        tmp &= ~ DDS_DAC_AMPL_MASK();
        tmp |= DDS_DAC_AMPL_W(val);
        
        # write new register value
        self.carrier.writel( DDS_REG_DAC, tmp );
        
        self.ampl = val;
        
    # set output signal frequency
    def set_frequency(self, val):
        
        # this operations are required to compute
        # lut step value for the core
        tune = (1<<45)/self.sample_rate;
        tune *= val;
        
        tune = int( tune );
      
#       registers updated in fpga
        self.carrier.writel(DDS_REG_FREQ_HI, (tune >> 32) & 0xffffffff );
        self.carrier.writel(DDS_REG_FREQ_LO, tune & 0xffffffff);
        
        self.freq = val;
        
                
        