import commands


def spec_identification():

	dutnr = commands.getoutput("ls /sys/bus/fmc/devices/ | grep -iv adc | grep -v 100. | grep -e '.*-[0-9][0-9][0-9][0-9]$' | wc -w ")

	if int( dutnr ) == 1:
		dutbus_string=commands.getoutput("ls /sys/bus/fmc/devices/ | grep -iv adc | grep -v 100. | grep -e '.*-[0-9][0-9][0-9][0-9]$' | sed 's/.*-//'")    
		dutbus = int(dutbus_string[0:2],16)
	else:
		dutbus = -1;

	adcnr=commands.getoutput("ls /sys/bus/fmc/devices/ | grep -i adc | grep 100. | grep -e '.*-[0-9][0-9][0-9][0-9]$' | wc -w")
	if int( adcnr ) == 1:
		adcbus_string=commands.getoutput("ls /sys/bus/fmc/devices/ | grep -i adc | grep 100. | grep -e '.*-[0-9][0-9][0-9][0-9]$' | sed 's/.*-//'")
		adcbus = int(adcbus_string[0:2],16)
	else:
		adcbus = -1;

	return [ dutbus, adcbus ]


def dut_bus():
	print spec_identification()[0]

def adc_bus():
	print spec_identification()[1]