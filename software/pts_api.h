#ifndef __PTS_API_H
#define __PTS_API_H


void rf_writel(struct wr_rf_device *dev, uint32_t data, uint32_t addr);
uint32_t rf_readl(struct wr_rf_device *dev, uint32_t addr);

#endif
