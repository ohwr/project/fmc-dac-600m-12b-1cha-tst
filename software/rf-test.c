#include <stdio.h>

#include "rf-lib.h"
#include "speclib.h"

#define SAMPLES 1000000

int main(int argc, char *argv[])
{
    struct wr_rf_device *dev_master, *dev_slave;
    void *card_master, *card_slave;
    double default_freq = 10e6;
//    int adc_master_buffer[SAMPLES];
//    int adc_slave_buffer[1000];
    int i;

    card_master = spec_open(2, -1);
    card_slave = spec_open(1, -1);
//    dev.base = 0x80000;

    if(!card_master || !card_slave)
    {
	dbg("SPEC open failed\n");
	return -1;
    }

    dev_master = rf_create(card_master,  0x80000);
    dev_slave = rf_create(card_slave,  0x80000);


//    printf("SDB ID A = %x\n", spec_readl(card_master, 0));
//    printf("SDB ID B = %x\n", spec_readl(card_slave, 0));

    
    rf_init(dev_slave, default_freq, RF_MODE_SLAVE, 11);
    sleep(2);
    rf_init(dev_master, default_freq, RF_MODE_MASTER, 11);



    struct rf_counters cnt_m, cnt_s;
    for(;;)
    {
	rf_get_counters(dev_master, &cnt_m);
	rf_get_counters(dev_slave, &cnt_s);

//	printf("ADC\tmaster\tslave\n");
//	read_adc(dev_master, adc_master_buffer, SAMPLES);
//	read_adc(dev_slave, adc_slave_buffer, 1000);

//	for( i=0; i < SAMPLES; i++ ){
////		printf("\t%d\t%d\n",adc_master_buffer[i],adc_slave_buffer[i] );
//		printf("%d\n",adc_master_buffer[i] );
//	}


//		if( !check_pll_lock( dev_master ) ){
//			printf( "PLL NOT locked" );
//
//		}

		printf( "PLL lock:%d\n", check_pll_lock( dev_master ) );
		if( check_pll_lock( dev_master ) )
				read_pi_value(dev_master);

//		if( getchar() == 'r' )
//		resync_pll( dev_master, dev_slave );

        printf("Master cnt: tx %d rx %d hits %d misses %d\n", cnt_m.tx, cnt_m.rx, cnt_m.hit, cnt_m.miss);
        printf("Slave cnt: tx %d rx %d hits %d misses %d\n", cnt_s.tx, cnt_s.rx, cnt_s.hit, cnt_s.miss);


	sleep(1);
    }

    spec_close(card_master);
    spec_close(card_slave);
    return 0;
}

