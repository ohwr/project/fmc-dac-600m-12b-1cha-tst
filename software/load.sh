#!/bin/bash

insmod /lib/modules/3.18.3-031803-generic/extra/fmc.ko
# sleep 1
insmod /lib/modules/3.18.3-031803-generic/extra/spec.ko

# sleep 5

# some magic to make script runable from any path
SCRIPTDIR='fmcdac600m12b1chadds\/software';
TARGETDIR='fmcdac600m12b1chadds\/syn\/spec_top.bit';

SPECLOADER='spec-sw/tools/spec-fwloader'

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
FWDIR=`echo ${DIR} | sed "s/${SCRIPTDIR}/${TARGETDIR}/"`

echo "Loading SPEC 1"
"${SPECLOADER}" -b 1 "${FWDIR}"
# echo "Loading SPEC 2"
# "${SPECLOADER}" -b 2 "${FWDIR}"

echo "Done"
sleep 1

./rf-test
