#include <stdio.h>

#include "rf-lib.h"
#include "speclib.h"
#include "regs/dds_regs.h"
#include "pts_api.h"



void rf_writel(struct wr_rf_device *dev, uint32_t data, uint32_t addr)
{
    spec_writel(dev->card, data, addr + dev->base);
}

uint32_t rf_readl(struct wr_rf_device *dev, uint32_t addr)
{
    return spec_readl (dev->card, addr + dev->base);
}


int test_communication( int bus, int base_addr ){

    void *card;
    struct wr_rf_device *dev;

    uint32_t tmp;

    card = spec_open( bus, -1);

    if( !card ){
        fprintf( stderr,"test_communication: Can't open SPEC\n" );
        return -1;
    }
    if(spec_load_bitstream(card, "../syn/spec_top.bit") < 0){
        fprintf(stderr, "Loader failure.\n");
        return -1;
    }
    // create structure with device informations
    dev = rf_create(card,  base_addr);

    tmp = spec_readl(dev->card, 0);
    dbg( "SDB signature = 0x%x\n", tmp );

    tmp = rf_readl(dev, DDS_REG_GW );
    dbg( "GW ID:%x\n", tmp );

//    rf_writel(dev, DDS_TEST_COMM_REG_W(DDS_TEST_COMM_LED_R), DDS_REG_TEST_COMM );
//      getchar();
//      rf_writel(dev, DDS_TEST_COMM_REG_W(DDS_TEST_COMM_LED_G), DDS_REG_TEST_COMM );
    rf_writel(dev, 1, DDS_REG_TEST_COMM );
    getchar();
    rf_writel(dev, 3, DDS_REG_TEST_COMM );
    getchar();
    rf_writel(dev, 0, DDS_REG_TEST_COMM );


    spec_close( card );

    return tmp;

}
