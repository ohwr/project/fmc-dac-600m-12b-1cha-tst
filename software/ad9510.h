/*
 * ad9510.h
 *
 *  Created on: Mar 24, 2015
 *      Author: gumas
 */

#ifndef AD9510_H_
#define AD9510_H_

#define WBGEN2_GEN_WRITE(value, offset, size) (((value) & ((1<<(size))-1)) << (offset))

// 0X0
#define SDO_INACTIVE(val)  			WBGEN2_GEN_WRITE(val, 7, 1)
#define LSB_FIRST(val) 				WBGEN2_GEN_WRITE(val, 6, 1)
#define SOFT_RESET(val) 			WBGEN2_GEN_WRITE(val, 5, 1)
#define LONG_INSTRUCTION(val)		WBGEN2_GEN_WRITE(val, 4, 1)

//0X4
#define A_COUNTER(val)				WBGEN2_GEN_WRITE(val, 0, 6)

//0X5
#define B_COUNTER_MSB(val)			WBGEN2_GEN_WRITE(val, 0, 5)

//OX6
#define B_COUNTER_LSB(val)			WBGEN2_GEN_WRITE(val, 0, 8)

//0X7
#define LOR_DEL(val)				WBGEN2_GEN_WRITE(val, 5, 2)
#define LOR_EN(val)					WBGEN2_GEN_WRITE(val, 2, 1)


//0X8
#define PFD_POLARITY(val)				WBGEN2_GEN_WRITE(val, 6, 1)
#define PLL_MUX_ON_STATUS(val)			WBGEN2_GEN_WRITE(val, 2, 4)
#define CP_MODE(val)					WBGEN2_GEN_WRITE(val, 0, 2)

//0X9
#define CP_CURRENT(val)					WBGEN2_GEN_WRITE(val, 4, 3)
#define RESET_R(val)					WBGEN2_GEN_WRITE(val, 2, 1)
#define RESET_N(val)					WBGEN2_GEN_WRITE(val, 1, 1)
#define RESET_ALL_CNTRS(val)			WBGEN2_GEN_WRITE(val, 0, 1)

//0X0A
#define B_BYPASS(val)			WBGEN2_GEN_WRITE(val, 6, 1)
#define PRESCALLER(val)			WBGEN2_GEN_WRITE(val, 2, 3)
#define POWER_DOWN(val)			WBGEN2_GEN_WRITE(val, 0, 2)

//0X0B
#define R_DIVIDER_MSB(val)			WBGEN2_GEN_WRITE(val, 0, 6)

//0X0C
#define R_DIVIDER_LSB(val)			WBGEN2_GEN_WRITE(val, 0, 8)


//0X0D
#define DIGIT_LOCK_DET_EN(val)			WBGEN2_GEN_WRITE(val, 6, 1)
#define DIGIT_LOCK_DET_WND(val)			WBGEN2_GEN_WRITE(val, 5, 1)
#define ANTIBACKSLASH_PLS_WDTH(val)		WBGEN2_GEN_WRITE(val, 0, 2)

//0X34 0X38
#define DEL_BYPASS(val)			WBGEN2_GEN_WRITE(val, 0, 1)

//0X35 0X39
#define RAMP_CAP(val)			WBGEN2_GEN_WRITE(val, 3, 3)
#define RAMP_CURR(val)			WBGEN2_GEN_WRITE(val, 0, 3)

//0X36 0X3A
#define FINE_DELAY(val)			WBGEN2_GEN_WRITE(val, 1, 5)

//0X3C 0X3D 0X3E 0X3F
#define OUTPUT_LEVEL(val)			WBGEN2_GEN_WRITE(val, 2, 2)
// THE SAME DEFINITION FOR REG 0X0A
//#define POWER_DOWN(val)				WBGEN2_GEN_WRITE(val, 0, 2)


//0X40 0X42 0X42 0X43
#define CMOS_INV(val)			WBGEN2_GEN_WRITE(val, 4, 1)
#define LOGIC_SEL(val)			WBGEN2_GEN_WRITE(val, 3, 1)
#define CMOS_OUTPUT_LEVEL(val)	WBGEN2_GEN_WRITE(val, 1, 2)
#define CMOS_OUTPUT_POWER(val)	WBGEN2_GEN_WRITE(val, 0, 1)

//0X45
#define CLKS_IN_PD(val)	WBGEN2_GEN_WRITE(val, 5, 1)
#define REFIN_PD(val)	WBGEN2_GEN_WRITE(val, 4, 1)
#define CLK_PLL_PD(val)	WBGEN2_GEN_WRITE(val, 3, 1)
#define CLK2_PD(val)	WBGEN2_GEN_WRITE(val, 2, 1)
#define CLK1_PD(val)	WBGEN2_GEN_WRITE(val, 1, 1)
#define SEL_CLK_IN(val)	WBGEN2_GEN_WRITE(val, 0, 1)


#define LOW_CYCLES(val)				WBGEN2_GEN_WRITE(val, 4, 4)
#define HIGH_CYCLES(val)			WBGEN2_GEN_WRITE(val, 0, 4)

#define BYPASS(val)					WBGEN2_GEN_WRITE(val, 7, 1)
#define NO_SYNC(val)				WBGEN2_GEN_WRITE(val, 6, 1)
#define FORCE(val)					WBGEN2_GEN_WRITE(val, 5, 1)
#define START_HL(val)				WBGEN2_GEN_WRITE(val, 4, 1)
#define PHASE_OFFSET(val)			WBGEN2_GEN_WRITE(val, 0, 4)

#define SET_FUNCTION_PIN(val)		WBGEN2_GEN_WRITE(val, 5, 2)
#define PD_SYNC(val)				WBGEN2_GEN_WRITE(val, 4, 1)
#define PD_ALL_REF(val)				WBGEN2_GEN_WRITE(val, 3, 1)
#define SYNC_REG(val)				WBGEN2_GEN_WRITE(val, 2, 1)
#define SYNC_SELECT(val)			WBGEN2_GEN_WRITE(val, 1, 1)
#define SYNC_ENABLE(val)			WBGEN2_GEN_WRITE(val, 0, 1)

#define UPDATE_REGISTERS(val)		WBGEN2_GEN_WRITE(val, 0, 1)


#endif /* AD9510_H_ */
