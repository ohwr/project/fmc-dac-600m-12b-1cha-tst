prg=$0
if [ ! -e "$prg" ]; then
  case $prg in
    (*/*) exit 1;;
    (*) prg=$(command -v -- "$prg") || exit;;
  esac
fi
dir=$(
  cd -P -- "$(dirname -- "$prg")" && pwd -P
) || exit
prg=$dir/$(basename -- "$prg") || exit 

prg=`echo "$prg" | sed 's/fmcdac600m12b1chadds.*/fmcdac600m12b1chadds\/dupa/'`

printf '%s\n' "$prg"