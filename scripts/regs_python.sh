#!/bin/bash
#grep dds_regs.h -e '^#define.*0x0.*'



echo -e "def WBGEN2_GEN_MASK(offset, size):\n\treturn (((1<<(size))-1) << (offset))"

echo -e "def WBGEN2_GEN_WRITE(value, offset, size):\n\treturn (((value) & ((1<<(size))-1)) << (offset))"
echo -e "def WBGEN2_GEN_READ(reg, offset, size):\n\treturn (((reg) >> (offset)) & ((1<<(size))-1))"

echo -e "\n\n"
echo -e "
def wbset(current, mask): 
\tcurrent |= mask;
\treturn current;

def wbclear(current, mask):
\tcurrent &= ~mask;
\treturn current;

def wbsetval(current, mask, val):
\treturn wbset(current, mask) if val else wbclear(current, mask)
"
# robi adresy rejestrow
# wybiera tylko wspisy: zaczynajace sie od define, znaki, 0x0, znaki
# sposrod wybranych wywala #define
# przed 0x0 dodaje znak roznosci
grep $1 -e '^#define.*0x0.*' | sed 's/#define //' | sed 's/0x0/= 0x0/'


echo -e "\n\n"
# grep dds_regs.h -e '^#define.*WBGEN2_GEN_MASK.*'

# wybiera wpisy dajace maski, one nam wystarcza
# zastepuje #define przez def (bedzie definicja funkcji w pytjonie)
# przed wartoscia define (czyli wbgen2_gen_mask) poprzedzona przez downolna ilosc(*) whistespace (\s)
	# wpisuje enter a pozniej tab
grep $1 -e '^#define.*WBGEN2_GEN_MASK.*[0-9])$' | sed 's/#define/def/' | sed 's/\s*WBGEN2/():\n\treturn WBGEN2/'
grep $1 -e '^#define.*WBGEN2_GEN_WRITE.*[0-9])$' | sed 's/#define/def/' | sed 's/\s*WBGEN2/:\n\treturn WBGEN2/'
grep $1 -e '^#define.*WBGEN2_GEN_READ.*[0-9])$' | sed 's/#define/def/' | sed 's/\s*WBGEN2/:\n\treturn WBGEN2/'
