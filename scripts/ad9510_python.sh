#!/bin/bash
#grep dds_regs.h -e '^#define.*0x0.*'



echo -e "def WBGEN2_GEN_WRITE(value, offset, size):\n\treturn (((value) & ((1<<(size))-1)) << (offset))"

echo -e "\n\n"



# grep dds_regs.h -e '^#define.*WBGEN2_GEN_MASK.*'

# wybiera wpisy dajace maski, one nam wystarcza
# zastepuje #define przez def (bedzie definicja funkcji w pytjonie)
# przed wartoscia define (czyli wbgen2_gen_mask) poprzedzona przez downolna ilosc(*) whistespace (\s)
	# wpisuje enter a pozniej tab
grep $1 -e '^#define.*WBGEN2_GEN_WRITE.*[0-9])$' | sed 's/#define/def/' | sed 's/\s*WBGEN2/:\n\treturn WBGEN2/'
