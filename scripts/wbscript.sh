#!/bin/bash

prg=$0
if [ ! -e "$prg" ]; then
  case $prg in
    (*/*) exit 1;;
    (*) prg=$(command -v -- "$prg") || exit;;
  esac
fi
dir=$(
  cd -P -- "$(dirname -- "$prg")" && pwd -P
) || exit
prg=$dir/$(basename -- "$prg") || exit 

top=`echo "$prg" | sed 's/fmcdac600m12b1chadds.*/fmcdac600m12b1chadds\//'`



"$top/scripts/wbgen2" -V "$top/gateware/rtl/dds_wb_slave.vhd" -H record -p "$top/gateware/rtl/dds_wbgen2_pkg.vhd" -s defines -C "$top/software/regs/dds_regs.h" "$top/gateware/rtl/dds_wb_slave.wb" 

bash "$top/scripts/regs_python.sh" "$top/software/regs/dds_regs.h" > "$top/python/regs_lib.py"
bash "$top/scripts/ad9510_python.sh" "$top/software/ad9510.h" > "$top/python/ad9510_lib.py"