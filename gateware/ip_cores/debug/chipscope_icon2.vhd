-------------------------------------------------------------------------------
-- Copyright (c) 2015 Xilinx, Inc.
-- All Rights Reserved
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor     : Xilinx
-- \   \   \/     Version    : 14.7
--  \   \         Application: XILINX CORE Generator
--  /   /         Filename   : chipscope_icon2.vhd
-- /___/   /\     Timestamp  : śro kwi 08 13:56:51 CEST 2015
-- \   \  /  \
--  \___\/\___\
--
-- Design Name: VHDL Synthesis Wrapper
-------------------------------------------------------------------------------
-- This wrapper is used to integrate with Project Navigator and PlanAhead

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY chipscope_icon2 IS
  port (
    CONTROL0: inout std_logic_vector(35 downto 0));
END chipscope_icon2;

ARCHITECTURE chipscope_icon2_a OF chipscope_icon2 IS
BEGIN

END chipscope_icon2_a;
