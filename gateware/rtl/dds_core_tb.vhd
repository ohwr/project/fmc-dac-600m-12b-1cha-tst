library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wr_fabric_pkg.all;
use work.wishbone_pkg.all;
use work.dds_wbgen2_pkg.all;
use work.gencores_pkg.all;
use work.genram_pkg.all;
use work.streamers_pkg.all;

entity dds_core_tb is
end dds_core_tb;


architecture arch of dds_core_tb is


	component dds_core_sim
	  
	  port (

	    -- Clocks & resets
	    clk_sys_i : in std_logic;
	    clk_dds_i : in std_logic;
	    clk_ref_i : in std_logic;
	    rst_n_i   : in std_logic;

	    -- Timing (WRC)
	    tm_link_up_i    : in std_logic := '1';
	    tm_time_valid_i : in std_logic;
	    tm_tai_i        : in std_logic_vector(39 downto 0);
	    tm_cycles_i     : in std_logic_vector(27 downto 0);

	    -- DDS Dac I/F (Maxim)
	    dac_n_o    : out std_logic_vector(13 downto 0);
	    dac_p_o    : out std_logic_vector(13 downto 0);
	    -- dac_pwdn_o : out std_logic;

	    -- AD9516 (SYS) and AD9510 (VCXO cleaner) PLL control
	    clk_dds_locked_i : in std_logic;

	    pll_vcxo_cs_n_o     : out std_logic;
	    pll_vcxo_function_o : out std_logic;
	    pll_vcxo_sdo_i      : in  std_logic;
	    pll_vcxo_status_i   : in  std_logic;

	    pll_sys_cs_n_o    : out std_logic;
	    -- pll_sys_refmon_i  : in  std_logic;
	    pll_sys_ld_i      : in  std_logic;
	    pll_sys_reset_n_o : out std_logic;
	    -- pll_sys_status_i  : in  std_logic;
	    pll_sys_sync_n_o  : out std_logic;

	    pll_sclk_o : out   std_logic;
	    pll_sdio_b : inout std_logic;

	    -- Phase Detector & ADC
	    -- pd_ce_o      : out   std_logic;
	    pd_lockdet_i : in    std_logic;
	    pd_clk_o     : out   std_logic;
	    pd_data_b    : inout std_logic;
	    pd_le_o      : out   std_logic;

	    adc_sdo_i : in  std_logic;
	    adc_sck_o : out std_logic;
	    adc_cnv_o : out std_logic;
	    adc_sdi_o : out std_logic;

	    -- slave_i : in  t_wishbone_slave_in;
	    slave_o : out t_wishbone_slave_out;

	    -- src_i : in  t_wrf_source_in;
	    src_o : out t_wrf_source_out;

	    -- snk_i : in  t_wrf_sink_in;
	    snk_o : out t_wrf_sink_out;

	    swrst_o      : out std_logic;
	    fpll_reset_o : out std_logic
	    );

	end component;





	signal clk_ref 				: std_logic := '0';
	signal clk_dds 				: std_logic := '0';
	
	signal reset 				: std_logic  := '1';


begin

	p_clk_ref: process
	begin
		clk_ref <= not clk_ref;
		wait for 8 ns;
	end process;


	p_clk_dds: process
	begin
		clk_dds <= not clk_dds;
		wait for 2 ns;
	end process;

	p_rst: process
	begin
		reset 	<= '0';
		wait for 100 ns;
		reset 	<= '1';
		wait for 100 ns;
		reset 	<= '0';
		wait;
	end process;


	cos: dds_core_sim
	  port map(

	    -- Clocks & resets
	    clk_sys_i 	=> clk_ref,
	    clk_dds_i	=> clk_dds,
	    clk_ref_i 	=> clk_ref,
	    rst_n_i   	=> not reset,

	    -- Timing (WRC)
	    tm_link_up_i    => '1',
	    tm_time_valid_i => '1',
	    tm_tai_i        => ( others => '0' ),
	    tm_cycles_i     => ( others => '0' ),

	    -- DDS Dac I/F (Maxim)
	    -- dac_n_o    : out std_logic_vector(13 downto 0);
	    -- dac_p_o    : out std_logic_vector(13 downto 0);
	    -- dac_pwdn_o : out std_logic;

	    -- AD9516 (SYS) and AD9510 (VCXO cleaner) PLL control
	    clk_dds_locked_i => '1',

	    -- pll_vcxo_cs_n_o     : out std_logic;
	    -- pll_vcxo_function_o : out std_logic;
	    pll_vcxo_sdo_i     => '1',
	    pll_vcxo_status_i  => '1',

	    -- pll_sys_cs_n_o    : out std_logic;
	    -- pll_sys_refmon_i  : in  std_logic;
	    pll_sys_ld_i      => '1',
	    -- pll_sys_reset_n_o : out std_logic;
	    -- pll_sys_status_i  : in  std_logic;
	    -- pll_sys_sync_n_o  : out std_logic;

	    -- pll_sclk_o : out   std_logic;
	    -- pll_sdio_b : inout std_logic;

	    -- Phase Detector & ADC
	    -- pd_ce_o      : out   std_logic;
	    pd_lockdet_i => '1',
	    -- pd_clk_o     : out   std_logic;
	    -- pd_data_b    : inout std_logic;
	    -- pd_le_o      : out   std_logic;

	    adc_sdo_i 		=> '1'
	    -- adc_sck_o : out std_logic;
	    -- adc_cnv_o : out std_logic;
	    -- adc_sdi_o : out std_logic;

	    -- slave_i : in  t_wishbone_slave_in;
	    -- slave_o : out t_wishbone_slave_out;

	    -- src_i : in  t_wrf_source_in;
	    -- src_o : out t_wrf_source_out;

	    -- snk_i : in  t_wrf_sink_in;
	    -- snk_o : out t_wrf_sink_out;

	    -- swrst_o      : out std_logic;
	    -- fpll_reset_o : out std_logic
	    );



end arch;