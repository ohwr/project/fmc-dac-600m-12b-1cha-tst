`timescale 1ns/1ps

module dds_quad_channel 
(
 clk_i,  
 rst_n_i,
 acc_i,
 acc_o,
 dreq_i,
 tune_i,
 tune_load_i,
 acc_load_i,
 y0_o,
 y1_o,
 y2_o,
 y3_o);

   parameter integer g_acc_frac_bits     = 32;
   
   parameter integer g_output_bits    = 14;
   parameter integer g_lut_sample_bits= 18;
   parameter integer g_lut_slope_bits = 18;
   parameter integer g_interp_shift   = 7;
   parameter integer g_lut_size_log2  = 10;

   parameter integer g_dither_taps = 32'hD0000001;
   parameter integer g_dither_length = 32;
   
   localparam c_acc_bits = g_acc_frac_bits + g_lut_size_log2 + 1;
   localparam c_lut_cell_size = g_lut_sample_bits + g_lut_slope_bits;

   input             clk_i;
   input             rst_n_i;
   input             acc_load_i;
   input             tune_load_i;
   input [g_acc_frac_bits + g_lut_size_log2  : 0] acc_i;
   output reg [g_acc_frac_bits + g_lut_size_log2  : 0] acc_o;
   input [g_acc_frac_bits + g_lut_size_log2  : 0] tune_i;
   input                                          dreq_i;
   output wire [g_output_bits-1:0]                 y0_o, y1_o, y2_o, y3_o;
   
   wire [g_lut_size_log2-1:0] lut_addr[0:3];
   reg [c_lut_cell_size-1:0] lut_data[0:3];
   
   reg [c_acc_bits-1:0]       acc, acc_d0, acc_f[0:3], tune;

   wire [g_output_bits-1:0]    y[0:3];
   
   // tune seem to be phase step value
   // acc would then be current phase value
   always@(posedge clk_i)
     begin
        if(!rst_n_i)begin
           tune <= 0;
           acc <= 0;
        end else begin
           if(tune_load_i)
             tune <= tune_i;
           
           if(acc_load_i)
             acc <= acc_i;
           else if(dreq_i) begin
             acc <= acc + tune;
           
              acc_d0 <= acc;
              acc_o <= acc;
              
              // this core runs 1/4 of DDS DAC's frequency
              // so phase is divided into 4 parts
              // differing of 1/4 of tune step
              // each subphase value is fed into separate dds channel
              acc_f[0] <= acc_d0;
              acc_f[1] <= acc_d0 + (tune >> 2);
              acc_f[2] <= acc_d0 + (tune >> 1);
              acc_f[3] <= acc_d0 + (tune >> 2) + (tune >> 1);
           end
           
        end // else: !if(!rst_n_i)
     end // always@ (posedge clk_i)

   generate
      genvar i;
      
      for(i=0;i<4;i=i+1)
        begin
           dds_stage 
            #(
              .g_acc_frac_bits(g_acc_frac_bits),
              .g_output_bits(g_output_bits),
              .g_lut_size_log2(g_lut_size_log2),
              .g_dither_init_value(i*1234567)
              )
           U_Stage_X 
            (
             .clk_i(clk_i),
             .rst_n_i(rst_n_i),
             // current phase
             .acc_i(acc_f[i]),
             // signal output
             .y_o(y[i]),
             // looks lke module enable (allways active in this design)
             .dreq_i(dreq_i),
             
             // LUT input
             .lut_addr_o(lut_addr[i]),
             // LUT outpu
             .lut_data_i(lut_data[i])
             );
        end // for (i=0;i<4;i++)
   endgenerate


   // two dimentional register declaration
   // actually lut_init.v is 2 dimentional
   // 1024 words of 36 bit
   // these probably are 1024 samples with 36 bit precision
   reg [g_lut_sample_bits + g_lut_slope_bits-1:0]    lut01[0:2**g_lut_size_log2-1];
   reg [g_lut_sample_bits + g_lut_slope_bits-1:0]    lut23[0:2**g_lut_size_log2-1];

`include "lut_init.v"
   
   // this is initialization of two LUT's
   // one is called lut01 the other is lut23
   // INIT_LUT( 01 ) seems to be a macro initializing lut01 register 
   // number is concatenated into name
   initial begin
      `INIT_LUT(01)
      `INIT_LUT(23)
   end
   
   // there are 4 DDS channes
   // this time there is copy paste insted of generate loop
   always@(posedge clk_i)
     lut_data[0] <= lut01[lut_addr[0]];
   always@(posedge clk_i)
     lut_data[1] <= lut01[lut_addr[1]];
   always@(posedge clk_i)
     lut_data[2] <= lut23[lut_addr[2]];
   always@(posedge clk_i)
     lut_data[3] <= lut23[lut_addr[3]];

  // vector assigned to non vector output
   assign y0_o = y[0];
   assign y1_o = y[1];
   assign y2_o = y[2];
   assign y3_o = y[3];

endmodule // dds_quad_channel

                         