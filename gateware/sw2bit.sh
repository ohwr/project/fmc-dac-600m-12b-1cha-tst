#!/bin/bash


cd software/wrpc-sw/

# make

cd ../..

. /opt/Xilinx/14.7/ISE_DS/settings64.sh
export XILINXD_LICENSE_FILE=2100@192.168.95.253
export LC_NUMERIC="en_US.UTF-8"

NAME="wr_syntef_"
NAME+=`date '+%Y_%m_%d__%H_%m'`
NAME+=".bit"
data2mem -bm syn/fpga_xst_bd.bmm -bd  software/wrpc-sw/wrc.elf -bt syn/spec_top.bit -o b "output/${NAME}"

rm "output/wr_syntef.bit"
cp "output/${NAME}" "output/wr_syntef.bit"

