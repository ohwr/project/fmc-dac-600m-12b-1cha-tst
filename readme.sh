# It is recommended to clone repository with parameter --depth=1 since older versions contain big redundant file.

# Whole software was tested under Ubuntu 14.04 LTS with kernel 3.13.


# clone repository to ~/pts/test/fmcdac600m12b1chadds

# in order to run fmcdac600m12b1chadds tests you need 
# cp210x and common directories from pts repository
# you may either copy whole pts repository or use a coppy attached in fmcdac600m12b1chadds repository

# if you do not want to clone pts repository:
# mv ptsfiles/* ../../

######################################
######################################
##### Package installation ##########
######################################
######################################

sudo apt install python-pip
pip install --user parse==1.11.1

######################################
######################################
##### Getting all submodules ##########
######################################
######################################


git submodule init
git submodule update

######################################
######################################
######## Compiling software ##########
######################################
######################################

cd software
make clean
make


######################################
######################################
##### Setting up PTS  ################
######################################
######################################

# moving test environment to pts directory
mv pts_fmcdac600m12b1chadds ~/pts/test/fmcdac600m12b1chadds

# setting up main scripts in pts directory
cp ~/pts/test/fmcdac600m12b1chadds/fmcdac600m12b1chadds.sh ~/pts/fmcdac600m12b1chadds.sh
cp ~/pts/test/fmcdac600m12b1chadds/fmcdac600m12b1chadds.sh ~/pts/fmcdds.sh
cp ~/pts/test/fmcdac600m12b1chadds/ptsDacDDS.py ~/pts/ptsDacDDS.py


######################################
######################################
########## Gateware   ################
######################################
######################################

# Repository contain binary file with SPEC configuration so there is no need to compile it again.
# If it were necessary 
# Project file for Xilinx ISE 14.7 is located in gateware/syn/pts_fmcdds.xise


######################################
######################################
###### Test configuration ############
######################################
######################################

# Since completion of test02 and part of test03 require small modifications in FmcDac board 
# fmcdac600m12b1chadds.sh script doesn't run test02.
# test03 contain define TEST_RAW_DAC set to False.

######################################
######################################
#### Setting up SPEC firmware #######
#######################################
######################################

# !!!!!!!!!!!!!!!!!!!
# You have to download spec "golden-firmware"
# latest version can be found on OHWR for example:
# http://www.ohwr.org/attachments/download/1756/spec-init.bin-2012-12-14
# I have also added one to this repo
# It has to be put in /lib/firmware/fmc

sudo mkdir /lib/firmware/fmc
sudo cp software/spec-init.bin-2012-12-14 /lib/firmware/fmc/spec-init.bin

By now you should be able to mount spec devices

# First check that spec is visible as PCI device 
lspci

# should give something like
# 01:00.0 Non-VGA unclassified device: CERN/ECP/EDU Device 018d (rev 03)
# 02:00.0 Non-VGA unclassified device: CERN/ECP/EDU Device 018d (rev 03)

