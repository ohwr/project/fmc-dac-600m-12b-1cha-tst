#!/bin/bash


rmmod fmc_adc_100m14b   2> /dev/null
rmmod zio               2> /dev/null
rmmod rawrabbit         2> /dev/null
rmmod spec              2> /dev/null
rmmod fmc               2> /dev/null


prg=$0
if [ ! -e "$prg" ]; then
  case $prg in
    (*/*) exit 1;;
    (*) prg=$(command -v -- "$prg") || exit;;
  esac
fi
dir=$(
  cd -P -- "$(dirname -- "$prg")" && pwd -P
) || exit
prg=$dir/$(basename -- "$prg") || exit 

top=`dirname -- "$prg"`

insmod "$top/software/fmc-bus/kernel/fmc.ko" 
insmod "$top/software/spec-sw/kernel/spec.ko" "fw_name=../../$top/gateware/spec-init.bin-2012-12-14"
insmod "$top/software/fmc-adc-100m14b4cha-sw/zio/zio.ko"
insmod "$top/software/fmc-adc-100m14b4cha-sw/kernel/fmc-adc-100m14b.ko" "gateware=../../$top/gateware/spec-fmc-adc-v4.0.bin"


LOGDIR="$top/log_fmcdac600m12b1chadds"

mkdir -p "$LOGDIR"
mkdir -p "$LOGDIR/eeprom"
mkdir -p "$LOGDIR/tmp"


serial=$1
if [ x$1 = x"" ]; then
	echo -n "Please scan CERN serial number bar-code, then press [ENTER]: "
        read serial
fi

if [ x$serial = x"" ]; then
	serial=0000
fi

extra_serial=$2
if [ x$2 = x"" ]; then
	echo -n "If needed input extra serial number and press [ENTER] OR just press [ENTER]: "
        read extra_serial
fi

if [ x$extra_serial = x"" ]; then
	extra_serial=0000
fi

echo " "

nb_test_limit=2
nb_test=1

while [ "$nb_test" -le "$nb_test_limit" ]
do
    echo "--------------------------------------------------------------"
    echo "Test series run $nb_test out of $nb_test_limit"
    echo " "

    sudo ./ptsDacDDS.py -b FmcDac600m12b1chaDds -s $serial -e $extra_serial -t./python -l $LOGDIR 00 01 02 03 04 05 08 10 11 07

    if [ "$nb_test" != "$nb_test_limit" ]
    then
        echo " "
        echo -n "Do you want to run the test series again [y,n]? "
        read reply
        if [ "$reply" != "y" ]
        then
            break
        fi
    fi
    nb_test=$(($nb_test+1))
done

echo "--------------------------------------------------------------"
echo " "
echo -n "End of the test, do you want to switch the computer OFF? [y,n]"
read reply
if [ "$reply" = "y" ]
then
    sudo halt
fi
